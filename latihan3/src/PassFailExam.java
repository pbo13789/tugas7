public class PassFailExam extends PassFailActivity{
    int numberOfQuestions,numberMissed;
    double pointsPerQuestion;

    PassFailExam(int questions,int missed,double minPassing) {
        super(minPassing);
        this.numberOfQuestions = questions;
        this.numberMissed = missed;
        this.pointsPerQuestion = minPassing;
        //TODO Auto-generated constructor stub
    }
    
    public int getNumberMissed() {
        System.out.println("Number Missed : " + this.numberMissed);
        return this.numberMissed;
    }

    public double getPointsEach() {
        System.out.println("Point Each : " + this.pointsPerQuestion);
        return this.pointsPerQuestion;
    }
}
