public class Main {
    public static void main(String[] args) {

        GradeActivity GA = new GradeActivity("Noval");
        FinalExam FE = new FinalExam(5, 2);
        PassFailActivity PFA = new PassFailActivity(5);
        PassFailExam PFE = new PassFailExam(11, 6, 3);
        
        GA.setScore(5);
        GA.getScore();
        GA.getGrade();

        
        FE.getPointsPerQuestion();
        FE.getNumberMissed();

        PFA.getGrade();

        PFE.getNumberMissed();
        PFE.getPointsEach();
    }
}
