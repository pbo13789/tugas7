
public class FinalExam extends GradeActivity{

        int numberOfQuestions,numberMissed;
        double pointsPerQuestion;

        FinalExam(int number, int missed){
            this.numberOfQuestions = number;
            this.numberMissed = missed;
        }

        public double getPointsPerQuestion() {
            pointsPerQuestion = numberOfQuestions;
            System.out.println("Nomor : " + pointsPerQuestion);
            return pointsPerQuestion;
        }
    
        public int getNumberMissed() {
            int miss = this.numberMissed;
            System.out.println("Missed : " + miss);
            return miss;
        }
}
