import java.util.*;
public class GradeActivity {
    Scanner inp = new Scanner(System.in);
    double score;
    String name;

    GradeActivity(){}

    GradeActivity(String n){
        this.name = n;
        System.out.println("Nama : " + n);
    }

    public void setScore(double s){
        this.score = s;
    }

    public double getScore() {
        return this.score;
    }

    public char getGrade() {
        String name = this.name;
        if (name != null) {
            char[] grade = name.toCharArray();
            char firstChar = grade[0];
            System.out.println("Karakter pertama dari nama : " + firstChar);
            return firstChar;
        } else {
            System.out.println("Nama tidak ditemukan");
            return ' ';
        }
    }    
}
